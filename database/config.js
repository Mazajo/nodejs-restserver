const moongose = require('mongoose');


const dbConnection = async() => {

    try {

        await moongose.connect(process.env.MONGO_DB_CONNECT, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        });

        console.log('Conectado');

    } catch (error) {

        console.log(error);
        throw new Error('Error en la conexion');

    }


}

module.exports = {
    dbConnection
}