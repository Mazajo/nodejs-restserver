const usuario = require('../models/usuario');

const { Usuario, Categoria, Roles, Producto } = require('../models');


const esValidoRol = async(role = '') => {

    const existeRol = await Roles({ role });
    if (!existeRol) {
        throw new Error(`El rol ${ role } no está registrado en nuestra BD`);
    }
}

const emailExiste = async(correo = '') => {

    const existeEmail = await Usuario.findOne({ correo });
    if (existeEmail) {
        throw new Error(`El email: ${ correo }, ya esta registrado`);
    }
}

const existeUsuario = async(id) => {

    const usuarioExiste = await Usuario.findById(id);
    if (!usuarioExiste) {
        throw new Error(`El id: ${id} no existe en la BD`);
    }
}


const existeCategoria = async(id) => {
    const categoriaExiste = await Categoria.findById(id);
    if (!categoriaExiste) {
        throw new Error(`La Categoria ya existe en la BD`);
    }
}

const existeProducto = async(id) => {
    const productoExiste = await Producto.findById(id);
    if (!productoExiste) {
        throw new Error(`El Producto ya existe en la BD`);
    }
}



module.exports = {
    esValidoRol,
    emailExiste,
    existeUsuario,
    existeCategoria,
    existeProducto

}