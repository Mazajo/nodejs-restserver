const { response } = require('express');
const jwt = require('jsonwebtoken');


const Usuario = require('../models/usuario');

const validarJWT = async(req = response, res = response, next) => {

    const token = req.header('token');

    if (!token) {
        return res.status(401).json({
            msg: 'No hay token en la petición'
        });
    }

    try {
        const { uid } = jwt.verify(token, process.env.MYSECRETTOKEN);

        const usuario = await Usuario.findById(uid);

        if (!usuario) {
            return res.status(401).json({
                msg: 'Usuario no existe en DB'
            });
        }

        if (!usuario.estado) {
            return res.status(401).json({
                msg: 'Usuario se encuentra en estado: false'
            });
        }

        req.usuario = usuario;

        next();
    } catch (error) {
        console.log(error);
        return res.status(401).json({
            msg: 'Token no válido'
        });

    }




}

module.exports = {
    validarJWT
}