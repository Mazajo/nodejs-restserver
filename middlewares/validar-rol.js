const { response } = require("express");
const roles = require("../models/roles");
const usuario = require("../models/usuario");




const validarAdminRol = (res = response, req, next) => {

    if (!req.usuario) {
        return res.status(500).json({
            msg: 'El usuario debe auntenticarse primero'
        });
    }

    const { role, nombre } = req.usuario
    if (!role == 'ADMIN_ROLE') {
        return res.status(401).json({
            msg: `${ nombre } no es Administrador `
        });
    }


    next();
}

const tieneRol = (req, res = response, next) => {

    if (!req.usuario) {
        return res.status(500).json({
            msg: 'El usuario debe auntenticarse primero'
        });
    }

    if (!roles.includes(req.usuario.role)) {
        return res.status(401).json({
            msg: `se debe ser uno de estos roles ${ roles } para poder ejecutar esta operaciones`
        });
    }

    next();
}


module.exports = {
    validarAdminRol,
    tieneRol
}