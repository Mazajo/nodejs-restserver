 const { Router } = require('express');
 const { check } = require('express-validator');


 const { usuariosGet, usuariosPost, usuariosPut, usuariosPatch, usuariosDelete } = require('../controllers/user');


 const { validarCampos } = require('../middlewares/validar_campos');
 const { validarJWT } = require('../middlewares/validar-jwt');
 const { validarAdminRol, tieneRol } = require('../middlewares/validar-rol');



 const { esValidoRol, emailExiste, existeUsuario } = require('../helpers/validar_db');
 const router = Router();


 router.get('/', usuariosGet);

 router.post('/', [
     check('nombre', 'El nombre es obligatorio').not().isEmpty(),
     check('contrasena', 'La contraseña debe ser manoyr a 6 caracteres').isLength({ min: 6 }),
     check('correo', 'El correo no es valido').isEmail(),
     check('correo').custom(emailExiste),
     //Este check es para colocar un rol o 2 
     //check('role', 'No es un rol valido').isIn(['ADMIN_ROLE', 'USER_ROLE']),
     check('role').custom(esValidoRol),
     validarCampos
 ], usuariosPost);

 router.put('/:id', [
     check('id', 'no es un id valido').isMongoId(),
     check('id').custom(existeUsuario),
     check('role').custom(esValidoRol),
     validarCampos
 ], usuariosPut);

 router.patch('/', usuariosPatch);

 router.delete('/:id', [
     validarJWT,
     tieneRol,
     //validarAdminRol,
     check('id', 'no es un id valido').isMongoId(),
     check('id').custom(existeUsuario),
     validarCampos
 ], usuariosDelete);


 module.exports = router;