const { Router } = require('express');
const { check } = require('express-validator');
const { login, googleSingIn } = require('../controllers/auth');
const { validarCampos } = require('../middlewares/validar_campos');


const router = Router();

router.post('/login', [
    check('correo', 'EL correo es obligatorio').isEmail(),
    check('contrasena', 'la contraseña es obligatorio').not().isEmpty(),
    validarCampos
], login);

router.post('/google', [
    check('id_token', 'la contraseña es obligatorio').not().isEmpty(),
    validarCampos
], googleSingIn);



module.exports = router;