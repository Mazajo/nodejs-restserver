const { Router } = require('express');
const { check } = require('express-validator');
const { crearCategoria, obtenerCategoria, obtenerCategorias, actualizarCategoria, borrarCategoria } = require('../controllers/categorias');
const { existeCategoria } = require('../helpers/validar_db');
const { validarJWT } = require('../middlewares/validar-jwt');
const { validarAdminRol } = require('../middlewares/validar-rol');
const { validarCampos } = require('../middlewares/validar_campos');


const router = Router();

router.get('/', obtenerCategorias);

router.get('/:id', [
    check('id', 'No es un id Valido').isMongoId(),
    check('id').custom(existeCategoria),
    validarCampos
], obtenerCategoria);


router.post('/', [
    validarJWT,
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    validarCampos
], crearCategoria);


router.put('/:id', [
    validarJWT,
    check('nombre', 'El nombre es requerido').not().isEmpty(),
    check('id').custom(existeCategoria),
    validarCampos
], actualizarCategoria);

router.delete('/:id', [
    validarJWT,
    validarAdminRol,
    check('id', 'El id no es valido en BD').isMongoId(),
    check('id').custom(existeCategoria),
    validarCampos
], borrarCategoria);


module.exports = router;