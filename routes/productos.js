const { Router } = require('express');
const { check } = require('express-validator');


const { validarCampos } = require('../middlewares/validar_campos');
const { validarJWT } = require('../middlewares/validar-jwt');
const { validarAdminRol } = require('../middlewares/validar-rol');
const { crearProducto, obtenerProducto, obtenerProductos, actualizarProducto, borrarProducto } = require('../controllers/productos');
const { existeProducto, existeCategoria } = require('../helpers/validar_db');




const router = Router();


router.get('/', obtenerProductos);

router.get('/:id', [
    check('id', 'No es un id de Mongo válido').isMongoId(),
    check('id').custom(existeProducto),
    validarCampos,
], obtenerProducto);

router.post('/', [
    validarJWT,
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('categoria', 'La categoria es obligatoria').isMongoId(),
    check('categoria').custom(existeCategoria),
    validarCampos
], crearProducto);

router.put('/:id', [
    validarJWT,
    // check('categoria','No es un id de Mongo').isMongoId(),
    check('id').custom(existeProducto),
    validarCampos
], actualizarProducto);


router.delete('/:id', [
    validarJWT,
    validarAdminRol,
    check('id', 'No es un id de Mongo válido').isMongoId(),
    check('id').custom(existeProducto),
    validarCampos,
], borrarProducto);

module.exports = router;