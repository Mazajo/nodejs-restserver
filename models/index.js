const Categoria = require('./categorias');
const Roles = require('../models/roles');
const Producto = require('../models/producto');
const Server = require('../models/server');
const Usuario = require('../models/usuario');

module.exports = {
    Categoria,
    Roles,
    Server,
    Usuario,
    Producto
}