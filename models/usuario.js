const { Schema, model } = require('mongoose');

const UsuarioShema = Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },

    correo: {
        type: String,
        unique: true,
        required: [true, 'El correo es obligatorio']
    },

    contrasena: {
        type: String,
        required: [true, 'La contraseña es requerida']

    },
    img: {
        type: String,
    },

    role: {
        type: String,
        required: true,
        enum: [true, 'ADMIN_ROLE', 'USER_ROLE']
    },

    estado: {
        type: Boolean,
        default: true
    },

    google: {
        type: Boolean,
        default: false
    }
});

UsuarioShema.methods.toJSON = function() {
    const { __v, contrasena, _id, ...usuario } = this.toObject();
    usuario.uid = _id;
    return usuario
}

module.exports = model('Usuario', UsuarioShema);