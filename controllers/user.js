const { response } = require('express');
const bcrip = require('bcryptjs');
const { Usuario } = require('../models');



const usuariosGet = async(req, res = response) => {

    const usuarios = await Usuario.find({ estado: true });
    const total = await Usuario.countDocuments();

    res.json({
        total,
        usuarios
    });
}

const usuariosPost = async(req, res = response) => {


    const { nombre, correo, contrasena, role } = req.body;
    const usuario = new Usuario({ nombre, correo, contrasena, role });


    const salt = bcrip.genSaltSync();
    usuario.contrasena = bcrip.hashSync(contrasena, salt);

    await usuario.save();

    res.json({
        usuario
    });
}

const usuariosPut = async(req, res = response) => {

    const { id } = req.params;
    const { _id, contrasena, google, ...resto } = req.body;

    if (contrasena) {

        const salt = bcrip.genSaltSync();
        resto.contrasena = bcrip.hashSync(contrasena, salt);
    }

    const usuario = await Usuario.findByIdAndUpdate(id, resto);

    res.status(200).json({
        usuario
    });
}

const usuariosPatch = (req, res = response) => {
    res.status(200).json({
        nombre: "tortas - controlador - Patch",
        id: 1
    });
}

const usuariosDelete = async(req, res = response) => {

    const { id } = req.params;

    //const uid = req.uid;

    //Para eliminar de la BD
    //const usuario = await Usuario.findByIdAndDelete(id);

    const usuario = await Usuario.findByIdAndUpdate(id, { estado: false });
    const usuarioAutenticado = req.usuario;
    res.json({
        usuario,
        usuarioAutenticado
    });
}



module.exports = {
    usuariosGet,
    usuariosPost,
    usuariosPut,
    usuariosPatch,
    usuariosDelete
}