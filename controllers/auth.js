const { response } = require('express');
const bcrip = require('bcryptjs');
const { generarJWT } = require('../helpers/generar-jwt');
const Usuario = require('../models/usuario');
const { googleVerify } = require('../helpers/google-verify');




const login = async(req, res = response) => {

    const { correo, contrasena } = req.body;

    try {
        //verificar el usuario
        const usuario = await Usuario.findOne({ correo });
        if (!usuario) {
            return res.status(400).json({
                msg: 'Usuario / Contraseña no existe en la BD'
            });
        }

        //Verificar su estado
        if (!usuario.estado) {
            return res.status(400).json({
                msg: "Usuario no activado"
            });
        }
        //Verificar contraseña
        const validarContrasena = bcrip.compareSync(contrasena, usuario.contrasena);
        if (!validarContrasena) {
            return res.status(400).json({
                msg: 'Password no valido'
            });
        }
        //Generar JWT
        const token = await generarJWT(usuario.id);

        res.json({
            msg: 'Login Ok',
            usuario,
            token
        });


    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Hable con el administrador'
        });
    }


}

const googleSingIn = async(req, res = response) => {

    const { id_token } = res.body;

    try {

        const { correo, nombre, img } = await googleVerify(id_token);

        let usuario = await Usuario.findOne({ correo });
        if (!usuario) {
            const data = {
                nombre,
                correo,
                contrasena,
                img,
                google: true
            }

            usuario = new Usuario(data);
            await usuario.save();

        }

        if (!usuario.estado) {
            return res.status(401).json({
                msg: 'Hable con el administrador'
            });
        }


        const token = await generarJWT(usuario.id);

        res.json({
            msg: 'Login de Google OK',
            usuario,
            token
        });


    } catch (error) {

        res.status(400).json({
            msg: 'Token de Google no valido'
        });
    }



}

module.exports = {
    login,
    googleSingIn
}